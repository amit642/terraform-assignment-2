#SUBNET_associate with route table
resource "aws_route_table_association" "pri-sa" {
count = "${length(var.subnet_asso_id}"   
subnet_id = "${element(var.subnet_asso_id)}"
  route_table_id = "${var.rt_id}"
}

