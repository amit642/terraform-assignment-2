variable "vpc_id" {
    description = "VPC id"
}

variable "cidr_in" {
    description = "CIDR for route table "
    type = "list"
    default = ["0.0.0.0/0"]
}

variable "gate_id" {
    description = "GATE id to used in route table(ex-igw or nat)"
    
}