#creat SUBNET
resource "aws_subnet" "subnet" {
 count = "${length(var.sub_cidr)}"
  vpc_id     = "${var.vpc_id}"
  cidr_block = "${element(var.sub_cidr,count.index)}"
availability_zone = "${element(var.az,count.index)}"
tags = "${merge(var.tags, map("name","${element(var.name,count.index)}"), map("sub_type","${var.sub_type}"))}"
}

