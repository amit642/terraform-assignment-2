
#variables of subnet
variable "vpc_id" {
    description = "ID of vpc "

}
variable "sub_cidr" {
description = "CIDR of SUBNET "
type ="list"
}

variable "az" {
    description = "Availibility zone of subnets"
    type ="list"
}

variable "tags" {
    description = "tag of subnet"
    type = "map"
    default = {}
}
variable "sub_type" {
    description = "Type of subnet i'e private or public"

}

variable "environment" {
    description = "working environment"
    default = "dev"
}

variable "owner" {
    description = "owner of subnet"
    default = "opstree"
}
variable "name" {
    description = "tag name"
    type ="list"
}


