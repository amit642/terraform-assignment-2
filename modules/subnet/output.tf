output "subnet_id" {
    description = "SUBNET ID"
    value = "${join(",",aws_subnet.subnet.*.id)}"
    }
