variable "aws_region" {
description = "AWS region "
default = "us-east-1"
}

variable "vpc_cidr" {
description = "CIDR of VPC "
default = "10.0.0.0/16"
}

variable "sub_pub_cidr" {
    description = "CIDR of public subnets"
    type = "list"
    default = ["10.0.1.0/24","10.0.2.0/24"]
}

variable "sub_pri_mg_cidr" {
    description = "CIDR of private management subnets"
    type = "list"
    default = ["10.0.3.0/24","10.0.4.0/24"]
}

variable "sub_pri_ap_cidr" {
    description = "CIDR of private application subnets"
    type = "list"
    default = ["10.0.5.0/24","10.0.6.0/24"]
}

variable "sub_pri_db_cidr" {
    description = "CIDR of private database  subnets"
    type = "list"
    default = ["10.0.7.0/24","10.0.8.0/24"]
}

variable "sub_pub_az" {
    description = "avaibility zone of public subnets"
    type = "list"
    default = ["us-east-1a","us-east-1b"]
}

variable "sub_pri_mg_az" {
    description = "avaibility zone of private management subnets"
    type = "list"
    default = ["us-east-1a","us-east-1b"]
}
variable "sub_pri_ap_az" {
    description = "avaibility zone of private application subnets"
    type = "list"
    default = ["us-east-1a","us-east-1b"]
}
variable "sub_pri_db_az" {
    description = "avaibility zone of private database subnets"
    type = "list"
    default = ["us-east-1a","us-east-1b"]
}

variable "sub_pub_name" {
    description = "name of public subnets"
    type = "list"
    default = ["pub-a","pub-b"]
}

variable "sub_pri_mg_name" {
    description = "name of private management subnets"
    type = "list"
    default = ["prim-a","prim-b"]
}
variable "sub_pri_ap_name" {
    description = "name of private application subnets"
    type = "list"
    default = ["pria-a","pria-b"]
}

variable "sub_pri_db_name" {
    description = "name of private database subnets"
    type = "list"
    default = ["pridb-a","pridb-b"]
}

variable "sub_pub_type" {
 description = "Type of subnet"
default= "public"
}

variable "sub_pri_mg_type" {
 description = "Type of subnet"
default= "management"
}

variable "sub_pri_ap_type" {
 description = "Type of subnet"
default= "application"
}

variable "sub_pri_db_type" {
 description = "Type of subnet"
default= "database"
}

#variables of rt
variable "pub_rt_cidr" {
    description = "cidr of public route table"
    default = ["0.0.0.0/0"]
}
variable "pri_rt_cidr" {
    description = "cidr of private route table"
    default = ["0.0.0.0/0"]
}