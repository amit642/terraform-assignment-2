provider "aws" {
region = "${var.aws_region}"
}

module "vpc" {
source = "../modules/vpc"
vpc_cidr = "${var.vpc_cidr}"
}

#creating subnet-2-public
module "pub" {
    source = "../modules/subnet"
    vpc_id = "${module.vpc.vpc_id}"
    sub_cidr = "${var.sub_pub_cidr}"
    az = "${var.sub_pub_az}"
    name = "${var.sub_pub_name}"
    sub_type = "${var.sub_pub_type}"
    tags = {
        environment = "dev"
        owner = "opstree"
    }
}


#creating subnet-private-management
 module "pri_mg" {
     source = "../modules/subnet"
     vpc_id = "${module.vpc.vpc_id}"
     sub_cidr = "${var.sub_pri_mg_cidr}"
     az = "${var.sub_pri_mg_az}"
     name = "${var.sub_pri_mg_name}"
     sub_type = "${var.sub_pri_mg_type}"
     tags = {
         environment = "management"
         owner = "opstree"
     }
 }

 #creating subnet-private-application
 module "pri_ap" {
     source = "../modules/subnet"
     vpc_id = "${module.vpc.vpc_id}"
     sub_cidr = "${var.sub_pri_ap_cidr}"
     az = "${var.sub_pri_ap_az}"
     name = "${var.sub_pri_ap_name}"
     sub_type = "${var.sub_pri_ap_type}"
     tags = {
         environment = "application"
         owner = "opstree"
     }
 }

 #creating subnet-private-db
 module "pri_db" {
     source = "../modules/subnet"
     vpc_id = "${module.vpc.vpc_id}"
     sub_cidr = "${var.sub_pri_db_cidr}"
     az = "${var.sub_pri_db_az}"
     name = "${var.sub_pri_db_name}"
     sub_type = "${var.sub_pri_db_type}"
     tags = {
         environment = "db"
         owner = "opstree"
     }
 }

#creating INTERNET-GATEWAY
module "igw" {
source = "../modules/igw"
vpc_id = "${module.vpc.vpc_id}"
}

module "route_table_public" {
    source = "../modules/rtable"
    vpc_id = "${module.vpc.vpc_id}"
    cidr_in = "${var.pub_rt_cidr}"
    gate_id = "${module.igw.igw_id}"
}
module "route_table_private" {
    source = "../modules/rtable"
    vpc_id = "${module.vpc.vpc_id}"
    cidr_in = "${var.pri_rt_cidr}"
    gate_id = "${module.nat.nat_id}"
}

module "sub_asso_rt_pub" {
    source = "../modules/sub_asso_rt"
    route_table_id = "${module.rtable.rt_pub_id}"
    subnet_asso_id = ["${element(split(",",module.pub.subnet_id),count.index)}]"
}